'use strict';

(function () {
  const myAccounts = window.data.myAccounts();
  const accountsList = document.querySelector('.accounts__list');
  const accountsItem = document.querySelector('#accounts-element').content;

  accountsList.innerHTML = null;
  myAccounts.forEach((account) => {
    const currentAccount = accountsItem.cloneNode(true);
    let accountLink = currentAccount.querySelector('.accounts__link');
    let accountName = currentAccount.querySelector('.accounts__account-name');
  
    accountName.textContent = account.name;
    accountLink.href = account.link;
    accountLink.classList.add(account.class);
    accountsList.appendChild(currentAccount);
  });
})();