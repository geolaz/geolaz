'use strict';

(function () {
  window.data = {
    mySkills() {
      return [
        'Python',
        'Kubernetes',
        'Shell Scripting',
        'Docker',
        'Docker-Compose',
        'Nexus',
        'Vagrant',
        'SonarQube',
        'Jenkins',
        'CI/CD',
        'Terraform',
        'Ansible',
        'S3',
        'PHP',
        'HTML5',
        'CSS3',
        'JavaScript',
        'SASS',
        'Zimbra',
        'Git',
        'Linux',
        'Ubuntu',
        'CentOS',
        'Joomla',
        'iRedMail',
        'DNS',
        'DHCP',
        'macOS',
        'Windows',
        'Active Directory',
        'RDP',
        'DOM',
        'Apache',
        'NGINX',
        'MySQL',
        'PostgreSQL',
        'MongoDB',
        'AWS',
        'etc.',
      ];
    },
    myAccounts() {
      return [
        {
          'name' : 'facebook',
          'link' : 'https://www.facebook.com/george.alex.lazar',
          'class' : 'facebook'
        },
        {
          'name' : 'twitter',
          'link' : 'https://twitter.com/geolaz_original',
          'class' : 'twitter'
        },
        {
          'name' : 'github',
          'link' : 'https://github.com/georgylazarev',
          'class' : 'github'
        },
        {
          'name' : 'gitlab',
          'link' : 'https://gitlab.com/geolaz',
          'class' : 'gitlab'
        },
        {
          'name' : 'linkedin',
          'link' : 'https://www.linkedin.com/in/geolaz',
          'class' : 'linkedin'
        },
        {
          'name' : 'instagram',
          'link' : 'https://www.instagram.com/geolazofficial',
          'class' : 'instagram'
        },
      ];
    }
  };
})();
